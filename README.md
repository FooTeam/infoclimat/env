# Mon Infoclimat

## Technologies Utilisées

Les technologies suivantes ont été utilisées pour développer ce projet :

- [Docker](https://www.docker.com/) : environnement de développement commun.
- [Laravel](https://laravel.com/) : framework PHP utilisé pour créer l'API qui fait les requêtes à la BDD.
- [SvelteKit](https://kit.svelte.dev/) : framework permettant de créer des applications web sans compromis sur les performances et le référencements.
- [TailwindUI](https://tailwindcss.com/) : framework CSS à vocation utilitaire (maintenu, contrairement à MaterialUI).

## Documentation technique

### Prérequis

- Docker & docker-compose
- php > 7
- composer
- npm
- [mkcert](https://github.com/FiloSottile/mkcert#installation)

### Installation
Initialisez le projet :

```sh
make install
```

Lancez le projet :

```sh
docker-compose -f docker-compose.preview.yml up
```

