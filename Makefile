# API
reset-api:
	docker-compose exec php php artisan key:generate
	docker-compose exec php php artisan cache:clear

# Web
restart-web:
	docker-compose restart node
	make attach-web

attach-web:
	docker-compose logs -f --no-log-prefix --tail=10 node

# Run the project
up:
	docker-compose up -d

# Installation
setup-https:
	mkcert -install
	cd docker/caddy/certs && mkcert infoclimat.localhost
	cd docker/caddy/certs && mkcert api.infoclimat.localhost

install:
	cd api && composer install --ignore-platform-reqs
	cd api &&	cp .env.example .env
	cd api &&	php artisan key:generate
	cd web &&	npm install
	make setup-https
